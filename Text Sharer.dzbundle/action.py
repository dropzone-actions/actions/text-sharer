# Dropzone Action Info
# Name: 				Text Sharer
# Description: 			Uploads text to paste.ee.
# Handles: 				Files, Text
# Creator: 				Kolja Nolte
# URL: 					https://www.kolja-nolte.com
# Events: 				Clicked, Dragged
# KeyModifiers: 		Command, Option, Control, Shift
# SkipConfig: 			No
# RunsSandboxed: 		Yes
# Version: 				0.0.1
# MinDropzoneVersion: 	3.5
# PythonPath: 			/usr/local/bin/python3

import time
import sys
import requests
import os

key = 'asnbiRlqn6fzllaTDvgExXONIc7nEirYYYrjjK3kd'
api_url = 'https://api.paste.ee/v1/pastes'


def upload():
	parameters = {
		'description': 'test',
		'sections':    [
			{
				'name':     'Snippet',
				'syntax':   'php',
				'contents': '<?php echo "Fuck!"; ?>'
			}
		]
	}

	headers = {
		'Content-Type': 'application/json',
		'X-Auth-Token': key
	}

	response = requests.post(
		url=api_url,
		json=parameters,
		headers=headers
	)

	if response.ok:
		data = response.json()

		sys.exit(data['link'])
	else:
		sys.exit('Could not create snippet.')


def dragged():
	print(items)

	dz.begin("Starting some task...")
	dz.determinate(True)

	dz.percent(10)
	time.sleep(1)
	dz.percent(50)
	time.sleep(1)
	dz.percent(100)

	dz.finish("Task Complete")

	dz.text("Here's some output which will be placed on the clipboard")


def clicked():
	dz.finish("You clicked me!")
	dz.url(False)

# Text Sharer

> **Latest version:** 0.0.1

With *Text Sharer*, you can publish text from your clipboard or by dragging and dropping it onto the action to paste.ee.

## Installation

1. Use `git clone <URL>`
   or [download](https://gitlab.com/dropzone-actions/actions/text-sharer/-/archive/master/text-sharer-master.zip)
   all files as an .zip archive.
2. Find the file with the extension `.dzbundle`.
3. Move this file to `~/Library/Application Support/Dropzone/Actions`.
4. Open Dropzone, click on the arrow in the top bar and drag and drop your action into *Add to Grid*.

## Usage

...

## Python Version

> Python 3.9.4

## Modules Used

* `os`
* `requests`
* `sys`
* `time`

**Note**: All libraries listed come with macOS. You don't have to install any of these.
